# Django imports.
from django.apps import AppConfig

__author__ = 'Jason Parent'


class CoreConfig(AppConfig):
    name = 'core'
