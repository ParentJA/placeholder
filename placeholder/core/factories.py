# Django imports.
from django.conf import settings

# Third-party imports.
import factory

__author__ = 'Jason Parent'


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker('user_name')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')

    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ('username', 'email',)


class PostFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    title = factory.Faker('text')
    body = factory.Faker('text')

    class Meta:
        model = 'old.Post'


class CommentFactory(factory.django.DjangoModelFactory):
    post = factory.SubFactory(PostFactory)
    name = factory.Faker('name')
    email = factory.Faker('email')
    body = factory.Faker('text')

    class Meta:
        model = 'old.Comment'


class AlbumFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    title = factory.Faker('text')

    class Meta:
        model = 'old.Album'


class PhotoFactory(factory.django.DjangoModelFactory):
    album = factory.SubFactory(AlbumFactory)
    title = factory.Faker('text')
    url = factory.Faker('url')
    thumbnail_url = factory.Faker('url')

    class Meta:
        model = 'old.Photo'


class TodoFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    title = factory.Faker('text')
    completed = factory.Faker('pybool')

    class Meta:
        model = 'old.Todo'
