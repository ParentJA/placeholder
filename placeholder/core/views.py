# Third-party imports.
from rest_framework import viewsets

# Local imports.
from .models import Post, Comment, Album, Photo, Todo
from .serializers import (
    PostSerializer, CommentSerializer, AlbumSerializer, PhotoSerializer, TodoSerializer
)
from .filters import PostFilter, CommentFilter, AlbumFilter, PhotoFilter, TodoFilter

__author__ = 'Jason Parent'


class PostView(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filterset_class = PostFilter
    lookup_url_kwarg = 'post_id'


class CommentView(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filterset_class = CommentFilter
    lookup_url_kwarg = 'comment_id'


class AlbumView(viewsets.ModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    filterset_class = AlbumFilter
    lookup_url_kwarg = 'album_id'


class PhotoView(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    filterset_class = PhotoFilter
    lookup_url_kwarg = 'photo_id'


class TodoView(viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    filterset_class = TodoFilter
    lookup_url_kwarg = 'todo_id'
