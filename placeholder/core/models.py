# Django imports.
from django.conf import settings
from django.db import models

__author__ = 'Jason Parent'


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=250)
    body = models.TextField()

    class Meta:
        default_related_name = 'posts'


class Comment(models.Model):
    post = models.ForeignKey('core.Post', on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=250)
    body = models.TextField()

    class Meta:
        default_related_name = 'comments'


class Album(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)

    class Meta:
        default_related_name = 'albums'


class Photo(models.Model):
    album = models.ForeignKey('core.Album', null=True, blank=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=250)
    url = models.URLField(max_length=250)
    thumbnail_url = models.URLField(max_length=250)

    class Meta:
        default_related_name = 'photos'


class Todo(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    completed = models.BooleanField(default=False)

    class Meta:
        default_related_name = 'todos'
