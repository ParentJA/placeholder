# Third-party imports.
import faker

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT
from rest_framework.test import APIClient, APITestCase

# Local imports.
from ..factories import PostFactory, CommentFactory, AlbumFactory, PhotoFactory, TodoFactory, UserFactory

fake = faker.Faker()

__author__ = 'Jason Parent'


class TestPost(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        posts = PostFactory.create_batch(3)
        response = self.client.get(reverse('old:post'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_retrieve(self):
        post = PostFactory.create()
        response = self.client.get(reverse('old:post', kwargs={'pk': post.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        user = UserFactory.create()
        response = self.client.post(reverse('old:post'), data={
            'user': user.id,
            'title': fake.text(),
            'body': fake.text(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        post = PostFactory.create()
        response = self.client.put(reverse('old:post', kwargs={'pk': post.pk}), data={
            'user': post.user.id,
            'title': fake.text(),
            'body': fake.text(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        post = PostFactory.create()
        response = self.client.delete(reverse('old:post', kwargs={'pk': post.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass


class TestComment(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        comments = CommentFactory.create_batch(3)
        response = self.client.get(reverse('old:comment'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_retrieve(self):
        comment = CommentFactory.create()
        response = self.client.get(reverse('old:comment', kwargs={'pk': comment.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        post = PostFactory.create()
        response = self.client.post(reverse('old:comment'), data={
            'post': post.id,
            'name': fake.name(),
            'email': fake.email(),
            'body': fake.text(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        comment = CommentFactory.create()
        response = self.client.put(reverse('old:comment', kwargs={'pk': comment.pk}), data={
            'post': comment.post.id,
            'name': fake.name(),
            'email': fake.email(),
            'body': fake.text(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        comment = CommentFactory.create()
        response = self.client.delete(reverse('old:comment', kwargs={'pk': comment.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass


class TestAlbum(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        albums = AlbumFactory.create_batch(3)
        response = self.client.get(reverse('old:album'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_list_filter_by_user(self):
        user1 = UserFactory.create()
        user2 = UserFactory.create()
        album1 = AlbumFactory.create(user=user1)
        album2 = AlbumFactory.create(user=user2)
        album3 = AlbumFactory.create(user=user2)
        response = self.client.get(reverse('old:album'), data={'user': user2.id})
        self.assertEqual(HTTP_200_OK, response.status_code)
        self.assertEqual(2, len(response.data))

    def test_retrieve(self):
        album = AlbumFactory.create()
        response = self.client.get(reverse('old:album', kwargs={'pk': album.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        user = UserFactory.create()
        response = self.client.post(reverse('old:album'), data={
            'user': user.id,
            'title': fake.text(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        album = AlbumFactory.create()
        response = self.client.put(reverse('old:album', kwargs={'pk': album.pk}), data={
            'user': album.user.id,
            'title': fake.text(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        album = AlbumFactory.create()
        response = self.client.delete(reverse('old:album', kwargs={'pk': album.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass


class TestPhoto(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        photos = PhotoFactory.create_batch(3)
        response = self.client.get(reverse('old:photo'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_retrieve(self):
        photo = PhotoFactory.create()
        response = self.client.get(reverse('old:photo', kwargs={'pk': photo.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        album = AlbumFactory.create()
        response = self.client.post(reverse('old:photo'), data={
            'album': album.id,
            'title': fake.text(),
            'url': fake.url(),
            'thumbnail_url': fake.url(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        photo = PhotoFactory.create()
        response = self.client.put(reverse('old:photo', kwargs={'pk': photo.pk}), data={
            'album': photo.album.id,
            'title': fake.text(),
            'url': fake.url(),
            'thumbnail_url': fake.url(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        photo = PhotoFactory.create()
        response = self.client.delete(reverse('old:photo', kwargs={'pk': photo.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass


class TestTodo(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        todos = TodoFactory.create_batch(3)
        response = self.client.get(reverse('old:todo'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_retrieve(self):
        todo = TodoFactory.create()
        response = self.client.get(reverse('old:todo', kwargs={'pk': todo.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        user = UserFactory.create()
        response = self.client.post(reverse('old:todo'), data={
            'user': user.id,
            'title': fake.text(),
            'completed': fake.pybool(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        todo = TodoFactory.create()
        response = self.client.put(reverse('old:todo', kwargs={'pk': todo.pk}), data={
            'user': todo.user.id,
            'title': fake.text(),
            'completed': fake.pybool(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        todo = TodoFactory.create()
        response = self.client.delete(reverse('old:todo', kwargs={'pk': todo.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass


class TestUser(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        posts = UserFactory.create_batch(3)
        response = self.client.get(reverse('old:post'))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_retrieve(self):
        user = UserFactory.create()
        response = self.client.get(reverse('old:user', kwargs={'pk': user.pk}))
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_create(self):
        response = self.client.post(reverse('old:user'), data={
            'name': fake.name(),
            'username': fake.user_name(),
            'email': fake.email(),
        })
        self.assertEqual(HTTP_201_CREATED, response.status_code)

    def test_update(self):
        user = UserFactory.create()
        response = self.client.put(reverse('old:user', kwargs={'pk': user.pk}), data={
            'name': fake.name(),
            'username': fake.user_name(),
            'email': fake.email(),
        })
        self.assertEqual(HTTP_200_OK, response.status_code)

    def test_destroy(self):
        user = UserFactory.create()
        response = self.client.delete(reverse('old:user', kwargs={'pk': user.pk}))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def tearDown(self):
        pass
