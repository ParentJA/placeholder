# Third-party imports.
from django_filters.rest_framework import FilterSet

# Local imports.
from .models import Post, Comment, Album, Photo, Todo

__author__ = 'Jason Parent'


class PostFilter(FilterSet):
    class Meta:
        model = Post
        fields = ('user',)


class CommentFilter(FilterSet):
    class Meta:
        model = Comment
        fields = ('post',)


class AlbumFilter(FilterSet):
    class Meta:
        model = Album
        fields = ('user',)


class PhotoFilter(FilterSet):
    class Meta:
        model = Photo
        fields = ('album',)


class TodoFilter(FilterSet):
    class Meta:
        model = Todo
        fields = ('user',)
