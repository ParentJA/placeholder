# Django imports.
from django.urls import path

# Local imports.
from .views import PostView, CommentView, AlbumView, PhotoView, TodoView

__author__ = 'Jason Parent'

urlpatterns = [
    path('posts/', PostView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='post'),
    path('posts/<int:post_id>/', PostView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='post'),
    path('comments/', CommentView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='comment'),
    path('comments/<int:comment_id>/', CommentView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='comment'),
    path('albums/', AlbumView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='album'),
    path('albums/<int:album_id>/', AlbumView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='album'),
    path('photos/', PhotoView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='photo'),
    path('photos/<int:photo_id>/', PhotoView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='photo'),
    path('todos/', TodoView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='todo'),
    path('todos/<int:todo_id>/', TodoView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='todo'),
]
