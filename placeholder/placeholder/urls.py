# Local imports.
from collections import namedtuple

# Third-party imports.
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from rest_framework import permissions

# Django imports.
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

__author__ = 'Jason Parent'

Include = namedtuple('Include', ['patterns', 'app_name'])

schema_view = get_schema_view(
    openapi.Info(
        title='Placeholder API',
        default_version='v1',
        description='The public API interface for Placeholder.',
        contact=openapi.Contact(name='Jason Parent', email='jparent@eab.com'),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='docs'),
    path('api/v1/auth/', include(Include(patterns='users.urls', app_name='users',))),
    path('api/v1/', include(Include(patterns='core.urls', app_name='core',))),
    path('', TemplateView.as_view(template_name='index.html')),
]
