# Django imports.
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Local imports.
from .models import User, UserSearchWord

__author__ = 'Jason Parent'

admin.site.register(User, UserAdmin)


@admin.register(UserSearchWord)
class UserSearchWordAdmin(admin.ModelAdmin):
    fields = ('id', 'user', 'word',)
    list_display = ('id', 'user', 'word',)
    list_select_related = ('user',)
    ordering = ('id',)
    readonly_fields = ('id', 'user', 'word',)
