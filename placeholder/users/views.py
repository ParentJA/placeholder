# Django imports.
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.forms import PasswordChangeForm

# Third-party imports.
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, views, viewsets
from rest_framework.response import Response

# Local imports.
from .filters import UserFilter
from .serializers import JoinSerializer, LoginSerializer, UserSerializer

__author__ = 'Jason Parent'


class JoinView(views.APIView):
    """Signs the user up for a new account."""

    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(request_body=JoinSerializer, responses={
        200: UserSerializer
    })
    def post(self, request):
        serializer = JoinSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(data=UserSerializer(user).data)


class LoginView(views.APIView):
    """Logs the user into the app."""

    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(request_body=LoginSerializer, responses={
        200: UserSerializer
    })
    def post(self, request):
        serializer = LoginSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(data=UserSerializer(user).data)


class LogoutView(views.APIView):
    """Logs the user out of the app."""

    permission_classes = (permissions.AllowAny,)

    def delete(self, request):
        if request.user.is_authenticated:
            logout(request)
        return Response(data=None, status=status.HTTP_204_NO_CONTENT)


class UserView(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filterset_class = UserFilter
    lookup_url_kwarg = 'user_id'
