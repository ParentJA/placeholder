# Django imports.
from django.contrib.postgres.search import SearchQuery, SearchRank, TrigramSimilarity
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.db.models.expressions import F

# Third-party imports.
from django_filters.rest_framework import CharFilter, FilterSet

__author__ = 'Jason Parent'


class UserFilter(FilterSet):
    query = CharFilter(field_name='query', method='filter_query')

    def filter_query(self, queryset, name, value):
        if not value:
            return queryset.all()
        return queryset.annotate(
            search_rank=SearchRank(F('search_vector'), SearchQuery(value)),
            trigram_similarity=TrigramSimilarity('usersearchword__word', value)
        ).filter(
            Q(search_rank__gte=0.1) |
            Q(trigram_similarity__gt=0.3)
        ).order_by(
            '-search_rank',
            'id'
        ).distinct('search_rank', 'id')

    class Meta:
        model = get_user_model()
        fields = ('query', 'is_staff',)
