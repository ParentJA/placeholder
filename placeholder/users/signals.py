# Django imports.
from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchVector
from django.db import connection
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

# Local imports.
from .models import UserSearchWord

__author__ = 'Jason Parent'


@receiver(post_save, sender=get_user_model())
def on_user_save(sender, instance, *args, **kwargs):
    # Handle user search vector.
    sender.objects.filter(pk=instance.id).update(search_vector=(
        SearchVector('username', weight='A') +
        SearchVector('email', weight='A') +
        SearchVector('first_name', weight='B') +
        SearchVector('last_name', weight='B')
    ))

    # Handle search words.
    UserSearchWord.objects.filter(user__id=instance.id).delete()
    with connection.cursor() as cursor:
        cursor.execute(f"""
            INSERT INTO users_usersearchword (user_id, word)
            SELECT {instance.id}, word FROM ts_stat('
              SELECT to_tsvector(''simple'', username) ||
                     to_tsvector(''simple'', first_name) ||
                     to_tsvector(''simple'', last_name) ||
                     to_tsvector(''simple'', email)
                FROM users_user
               WHERE id = {instance.id}
            ');
        """)


@receiver(post_delete)
def on_user_delete(sender, instance, *args, **kwargs):
    # Handle search words.
    UserSearchWord.objects.filter(user__id=instance.id).delete()
