# Django imports.
from django.contrib.auth import authenticate, get_user_model, login
from django.db.models import Q

# Third-party imports.
from rest_framework import exceptions, serializers

__author__ = 'Jason Parent'


class JoinSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True)
    first_name = serializers.CharField(write_only=True)
    last_name = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True)
    password = serializers.CharField(write_only=True)

    def validate(self, attrs):
        attrs = super().validate(attrs)

        # Check if user already exists.
        try:
            user = get_user_model().objects.get(Q(username=attrs['username']) | Q(email=attrs['email']))
        except get_user_model().DoesNotExist:
            return attrs
        else:
            if attrs['username'] == user.username:
                raise exceptions.ValidationError('A user with that username already exists.')
            if attrs['email'] == user.email:
                raise exceptions.ValidationError('A user with that email already exists.')

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = get_user_model()(**validated_data)
        user.set_password(password)
        user.save()

        # Log in user.
        login(request=self.context['request'], user=user)

        return user


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    def save(self, **kwargs):
        username = self.validated_data['username']
        password = self.validated_data['password']

        user = authenticate(request=self.context['request'], username=username, password=password)
        if user:
            login(request=self.context['request'], user=user)
        else:
            raise exceptions.ValidationError({'non_field_errors': [
                'Please enter a correct username and password. Note that both fields may be case-sensitive.'
            ]})

        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'is_staff',)
