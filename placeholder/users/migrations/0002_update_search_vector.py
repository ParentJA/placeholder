# Django imports.
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.db import migrations

__author__ = 'Jason Parent'


def update_search_vector(apps, schema_editor):
    User = apps.get_model('users', 'User')
    User.objects.all().update(search_vector=(
        SearchVector('username', weight='A') +
        SearchVector('email', weight='A') +
        SearchVector('first_name', weight='B') +
        SearchVector('last_name', weight='B')
    ))


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='search_vector',
            field=SearchVectorField(blank=True, null=True),
        ),
        migrations.AddIndex(
            model_name='user',
            index=GinIndex(fields=['search_vector'], name='search_vector_index'),
        ),
        migrations.RunPython(update_search_vector, elidable=True),
    ]
