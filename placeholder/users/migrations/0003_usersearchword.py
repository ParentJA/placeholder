# Django imports.
from django.conf import settings
from django.db import connection, migrations, models
import django.db.models.deletion

__author__ = 'Jason Parent'


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_update_search_vector'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserSearchWord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('word', models.TextField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        )
    ]
