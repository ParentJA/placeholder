# Django imports.
from django.contrib.postgres.operations import TrigramExtension
from django.db import connection, migrations

__author__ = 'Jason Parent'


def update_user_search_word(apps, schema_editor):
    User = apps.get_model('users', 'User')
    queries = [f"""
        INSERT INTO users_usersearchword (user_id, word)
        SELECT {user.id}, word FROM ts_stat('
          SELECT to_tsvector(''simple'', username) ||
                 to_tsvector(''simple'', first_name) ||
                 to_tsvector(''simple'', last_name) ||
                 to_tsvector(''simple'', email)
            FROM users_user
           WHERE id = {user.id}
        ');
    """ for user in User.objects.all()]
    with connection.cursor() as cursor:
        cursor.execute('\n'.join(queries))


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_usersearchword'),
    ]

    operations = [
        TrigramExtension(),
        migrations.RunSQL(sql="""
            CREATE INDEX IF NOT EXISTS user_search_word_trigram_index
                ON users_usersearchword
             USING gin (word gin_trgm_ops);
        """, elidable=True),
        migrations.RunPython(update_user_search_word, elidable=True),
    ]
