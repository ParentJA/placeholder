# Django imports.
from django.urls import path

# Local imports.
from .views import JoinView, LoginView, LogoutView, UserView

__author__ = 'Jason Parent'

urlpatterns = [
    path('join/', JoinView.as_view(), name='join'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('users/', UserView.as_view({
        'get': 'list',
        'post': 'create',
    }), name='user'),
    path('users/<int:user_id>/', UserView.as_view({
        'get': 'retrieve',
        'put': 'partial_update',
        'delete': 'destroy',
    }), name='user'),
]
