# Django imports.
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField
from django.db import models

__author__ = 'Jason Parent'


class User(AbstractUser):
    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ('first_name', 'last_name', 'email',)

    search_vector = SearchVectorField(null=True, blank=True)

    class Meta:
        indexes = [
            GinIndex(fields=['search_vector'], name='search_vector_index')
        ]
        ordering = ('id',)


class UserSearchWord(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    word = models.TextField()
