import { Data } from '@angular/router';

import { ReplaySubject } from 'rxjs';

export class ActivatedRouteStub {
  private subject = new ReplaySubject<Data>();

  constructor(initialData?: Data) {
    this.setData(initialData);
  }

  readonly data = this.subject.asObservable();

  setData(data?: Data): void {
    this.subject.next(data);
  }
}