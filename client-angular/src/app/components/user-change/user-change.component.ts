import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { User, UserService } from '../../services';

@Component({
  selector: 'app-user-change',
  templateUrl: './user-change.component.html',
  styleUrls: ['./user-change.component.css']
})
export class UserChangeComponent implements OnInit {
  user: User;
  userForm: FormGroup;
  nonFieldErrors: string[];
  fieldErrors: Map<string, string[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router, 
    private userService: UserService
  ) {
    this.userForm = new FormGroup({
      username: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required)
    });
    this.fieldErrors = new Map();
  }

  ngOnInit() {
    this.route.data.subscribe((data: {user: User}) => {
      this.user = data.user;
      this.userForm.patchValue(this.user);
    });
  }

  getFieldError(
    formControlName: string, 
    defaultMessage: string = 'This field may not be blank.'
  ): string {
    const errors = this.fieldErrors.get(formControlName);
    if (errors) {
      return errors.join(', ');
    }
    return defaultMessage;
  }

  isFieldInvalid(formControlName: string): boolean {
    const formControl = this.userForm.get(formControlName);
    return formControl.invalid && (formControl.dirty || formControl.touched);
  }

  onDelete(): void {
    this.userService.destroyUser(this.user.id).subscribe(data => {
      this.router.navigateByUrl('/user');
    });
  }

  onSave(): void {
    this.userService.updateUser(
      this.user.id,
      this.userForm.get('username').value,
      this.userForm.get('firstName').value,
      this.userForm.get('lastName').value,
      this.userForm.get('email').value
    ).subscribe(user => {
      this.router.navigateByUrl('/user');
    }, (data: HttpErrorResponse) => {
      const { username, first_name: firstName, last_name: lastName, email, ...others } = data.error;
      const errors = new Map(Object.entries({ username, firstName, lastName, email, ...others }));
      this.nonFieldErrors = errors.get('non_field_errors');
      if (this.nonFieldErrors) {
        errors.delete('non_field_errors');
      }
      this.fieldErrors = errors;
      this.fieldErrors.forEach((value, key, map) => {
        this.userForm.get(key).markAsDirty();
      });
    });
  }
}
