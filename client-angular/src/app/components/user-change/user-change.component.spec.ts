import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { of, throwError } from 'rxjs';

import { User, UserService } from '../../services';

import { UserChangeComponent } from './user-change.component';

import { ActivatedRouteStub } from '../../testing';

describe('UserChangeComponent', () => {
  const userData = {
    id: 1,
    username: 'test.user@example.com',
    first_name: 'Test',
    last_name: 'User',
    email: 'test.user@example.com'
  };
  const activatedRouteStub = new ActivatedRouteStub();
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const httpClientSpy = jasmine.createSpy();
  
  let activatedRouteSpy: jasmine.Spy;
  let userService: UserService;
  let component: UserChangeComponent;
  let fixture: ComponentFixture<UserChangeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChangeComponent ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        UserService
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    activatedRouteSpy = spyOn(activatedRouteStub.data, 'subscribe').and.callThrough();
    userService = TestBed.get(UserService);
    fixture = TestBed.createComponent(UserChangeComponent);
    component = fixture.componentInstance;
  });

  it('should load user on init', () => {
    expect(activatedRouteSpy).toHaveBeenCalled();
  });

  it('should navigate to the "/user" view on destroy success', () => {
    spyOn(userService, 'destroyUser').and.callFake(() => {
      return of(null);
    });
    component.user = User.create(userData);
    component.onDelete();
    expect(userService.destroyUser).toHaveBeenCalledWith(1);
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/user');
  });

  it('should navigate to the "/user" view on update success', () => {
    const {id, username, first_name: firstName, last_name: lastName, email} = userData;
    spyOn(userService, 'updateUser').and.callFake(() => {
      return of(User.create(userData));
    });
    component.user = User.create(userData);
    component.userForm.patchValue({id, username, firstName, lastName, email});
    component.onSave();
    expect(userService.updateUser).toHaveBeenCalledWith(...Object.values(userData));
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/user');
  });

  it('should mark form controls as dirty on update failure', () => {
    const formControls = [];
    for (const controlName in component.userForm.controls) {
      const control = component.userForm.get(controlName);
      spyOn(control, 'markAsDirty');
      formControls.push(control);
    }
    spyOn(userService, 'updateUser').and.callFake(() => {
      return throwError(new HttpErrorResponse({
        status: 400,
        statusText: 'Bad Request',
        error: '{"username": ["This field cannot be empty."]}'
      }));
    });
    component.user = User.create(userData);
    component.onSave();
    for (const control of formControls) {
      expect(control.markAsDirty).toHaveBeenCalled();
    }
  });
});
