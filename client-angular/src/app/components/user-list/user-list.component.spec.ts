import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { UserListComponent } from './user-list.component';

import { ActivatedRouteStub } from '../../testing';

describe('UserListComponent', () => {
  const activatedRouteStub = new ActivatedRouteStub();
  const routerSpy = jasmine.createSpy('Router');

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  });

  it('should load users on init', () => {
    const activatedRouteSpy = spyOn(activatedRouteStub.data, 'subscribe').and.callThrough();
    const fixture = TestBed.createComponent(UserListComponent);
    fixture.detectChanges();
    expect(activatedRouteSpy).toHaveBeenCalled();
  });
});
