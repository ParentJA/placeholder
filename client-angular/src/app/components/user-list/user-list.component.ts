import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { User, UserService } from '../../services';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  queryControl: FormControl;
  filters: any;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.queryControl = new FormControl('');
    this.filters = {};
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: {users: User[]}) => this.users = data.users);
  }

  onSearch(): void {
    const query = this.queryControl.value;
    const params = query.length > 0 ? Object.assign(this.filters, {query}) : this.filters;
    this.userService.listUsers(params).subscribe(users => {
      this.users = users;
    });
  }

  hasFilter(name: string): boolean {
    return name in this.filters;
  }

  setFilter(name: string, value: string): void {
    this.filters[name] = value;
    this.onSearch();
  }

  removeFilter(name: string): void {
    delete this.filters[name];
    this.onSearch();
  }

  trackByUsers(index: number, user: User): number {
    return user.id;
  }
}
