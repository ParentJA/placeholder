import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../services';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent {
  joinForm: FormGroup;
  nonFieldErrors: string[];
  fieldErrors: Map<string, string[]>;

  constructor(
    private router: Router, 
    private userService: UserService
  ) {
    this.joinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
    this.fieldErrors = new Map();
  }

  getFieldError(
    formControlName: string, 
    defaultMessage: string = 'This field may not be blank.'
  ): string {
    const errors = this.fieldErrors.get(formControlName);
    if (errors) {
      return errors.join(', ');
    }
    return defaultMessage;
  }

  isFieldInvalid(formControlName: string): boolean {
    const formControl = this.joinForm.get(formControlName);
    return formControl.invalid && (formControl.dirty || formControl.touched);
  }

  onSubmit(): void {
    this.userService.join(
      this.joinForm.get('username').value,
      this.joinForm.get('firstName').value,
      this.joinForm.get('lastName').value,
      this.joinForm.get('email').value,
      this.joinForm.get('password').value
    ).subscribe(user => {
      this.router.navigateByUrl('/admin');
    }, (data: HttpErrorResponse) => {
      const { username, first_name: firstName, last_name: lastName, email, password, ...others } = data.error;
      const errors = new Map(Object.entries({ username, firstName, lastName, email, password, ...others }));
      this.nonFieldErrors = errors.get('non_field_errors');
      if (this.nonFieldErrors) {
        errors.delete('non_field_errors');
      }
      this.fieldErrors = errors;
      this.fieldErrors.forEach((value, key, map) => {
        this.joinForm.get(key).markAsDirty();
      });
    });
  }
}
