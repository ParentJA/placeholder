import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { of, throwError } from 'rxjs';

import { User, UserService } from '../../services';

import { UserAddComponent } from './user-add.component';

describe('UserAddComponent', () => {
  const userData = {
    id: 1,
    username: 'test.user@example.com',
    first_name: 'Test',
    last_name: 'User',
    email: 'test.user@example.com'
  };
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const httpClientSpy = jasmine.createSpy();

  let userService: UserService;
  let component: UserAddComponent;
  let fixture: ComponentFixture<UserAddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAddComponent ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: Router, useValue: routerSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        UserService
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    userService = TestBed.get(UserService);
    fixture = TestBed.createComponent(UserAddComponent);
    component = fixture.componentInstance;
  });

  it('should navigate to the "/user" view on create success', () => {
    const {id, username, first_name: firstName, last_name: lastName, email} = userData;
    spyOn(userService, 'createUser').and.callFake(() => {
      return of(User.create(userData));
    });
    component.userForm.patchValue({id, username, firstName, lastName, email});
    component.onSave();
    expect(userService.createUser).toHaveBeenCalledWith(username, firstName, lastName, email);
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/user');
  });

  it('should mark form controls as dirty on create failure', () => {
    const formControls = [];
    for (const controlName in component.userForm.controls) {
      const control = component.userForm.get(controlName);
      spyOn(control, 'markAsDirty');
      formControls.push(control);
    }
    spyOn(userService, 'createUser').and.callFake(() => {
      return throwError(new HttpErrorResponse({
        status: 400,
        statusText: 'Bad Request',
        error: '{"username": ["This field cannot be empty."]}'
      }));
    });
    component.onSave();
    for (const control of formControls) {
      expect(control.markAsDirty).toHaveBeenCalled();
    }
  });
});
