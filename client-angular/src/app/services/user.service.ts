import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';

export class User {
  get user(): User {
    const data = localStorage.getItem('placeholder.user');
    if (data) {
      return User.create(JSON.parse(data));
    }
    return null;
  }

  constructor(
    public id: number,
    public username: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public isStaff: boolean
  ) {}

  static create(data: any): User {
    return new User(
      data.id,
      data.username,
      data.first_name,
      data.last_name,
      data.email,
      data.is_staff
    );
  }
}

@Injectable()
export class UserService {
  constructor(private client: HttpClient) {}

  listUsers(params?: HttpParams | { [param: string] : string | string[] }): Observable<User[]> {
    const url = '/api/v1/auth/users/';
    return this.client.get<User[]>(url, {params}).pipe(
      map(data => {
        return data.map((item: any) => User.create(item))
      })
    );
  }

  createUser(
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    isStaff: boolean = false
  ): Observable<User> {
    const url = '/api/v1/auth/users/';
    return this.client.post<User>(url, {
      username, 
      first_name: firstName, 
      last_name: lastName, 
      email,
      isStaff
    }).pipe(
      map(data => User.create(data))
    );
  }

  retrieveUser(userId: number): Observable<User> {
    const url = `/api/v1/auth/users/${userId}/`;
    return this.client.get<User>(url).pipe(
      map(data => User.create(data))
    );
  }

  updateUser(
    userId: number,
    username?: string,
    firstName?: string,
    lastName?: string,
    email?: string,
    isStaff?: boolean
  ): Observable<User> {
    const url = `/api/v1/auth/users/${userId}/`;
    return this.client.put<User>(url, {
      username, 
      first_name: firstName, 
      last_name: lastName, 
      email,
      isStaff
    }).pipe(
      map(data => User.create(data))
    );
  }

  destroyUser(userId: number): Observable<any> {
    const url = `/api/v1/auth/users/${userId}/`;
    return this.client.delete<any>(url);
  }

  join(
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    password: string
  ): Observable<User> {
    const url = '/api/v1/auth/join/';
    return this.client.post<User>(url, {
      username, 
      first_name: firstName, 
      last_name: lastName, 
      email,
      password
    }).pipe(
      map(data => User.create(data)),
      tap(data => localStorage.setItem('placeholder.user', JSON.stringify(data)))
    );
  }

  login(username: string, password: string): Observable<User> {
    const url = '/api/v1/auth/login/';
    return this.client.post<User>(url, {username, password}).pipe(
      map(data => User.create(data)),
      tap(data => localStorage.setItem('placeholder.user', JSON.stringify(data)))
    );
  }

  logout(): Observable<any> {
    const url = '/api/v1/auth/logout/';
    return this.client.delete<any>(url).pipe(
      finalize(() => localStorage.removeItem('placeholder.user'))
    );
  }
}
