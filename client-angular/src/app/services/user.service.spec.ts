import { HttpParams } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { User, UserService } from './user.service';

fdescribe('UserService', () => {
  const userData = {
    id: 1,
    username: 'test.user@example.com',
    first_name: 'Test',
    last_name: 'User',
    email: 'test.user@example.com'
  };

  let httpTestingController: HttpTestingController;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ 
        HttpClientTestingModule
      ],
      providers: [ 
        UserService 
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    userService = TestBed.get(UserService);
  });

  it('should list users', () => {
    const responseData = userData;
    userService.listUsers().subscribe(users => {
      expect(users).toEqual([
        User.create(responseData[0])
      ]);
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/');
    request.flush(responseData);
  });

  it('should list users by filters', () => {
    const query = new HttpParams().set('query', 'example');
    const responseData = userData;
    userService.listUsers(query).subscribe(users => {
      expect(users).toEqual([
        User.create(responseData[0])
      ]);
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/');
    request.flush(responseData);
  });

  it('should create a user', () => {
    const responseData = userData;
    userService.createUser(
      'test.user@example.com',
      'Test',
      'User',
      'test.user@example.com'
    ).subscribe(user => {
      expect(user).toEqual(User.create(responseData));
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/');
    request.flush(responseData);
  });

  it('should retrieve a user', () => {
    const responseData = userData;
    userService.retrieveUser(1).subscribe(user => {
      expect(user).toEqual(User.create(responseData));
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/1/');
    request.flush(responseData);
  });

  it('should update a user', () => {
    const responseData = userData;
    userService.updateUser(
      1,
      'test.user@example.com',
      'Test',
      'User',
      'test.user@example.com'
    ).subscribe(user => {
      expect(user).toEqual(User.create(responseData));
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/1/');
    request.flush(responseData);
  });

  it('should destroy a user', () => {
    const responseData = null;
    userService.destroyUser(1).subscribe(user => {
      expect(user).toBeNull();
    });
    const request = httpTestingController.expectOne('/api/v1/auth/users/1/');
    request.flush(responseData);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
