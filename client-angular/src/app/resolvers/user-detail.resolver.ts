import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { User, UserService } from '../services/user.service';

@Injectable()
export class UserDetailResolver implements Resolve<User> {
  constructor(private userService: UserService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const userId = parseInt(route.paramMap.get('userId'));
    return this.userService.retrieveUser(userId);
  }
}
