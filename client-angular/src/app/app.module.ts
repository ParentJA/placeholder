import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { 
  UserService 
} from './services';

import { 
  UserDetailResolver,
  UserListResolver
} from './resolvers';

import {
  AdminComponent,
  AlbumComponent,
  AppComponent,
  CommentComponent,
  JoinComponent,
  LoginComponent,
  PhotoComponent,
  PostComponent,
  TodoComponent,
  UserComponent,
  UserAddComponent,
  UserChangeComponent,
  UserListComponent
} from './components';

import { ROUTES } from './app.routes';

@NgModule({
  declarations: [
    AdminComponent,
    AlbumComponent,
    AppComponent,
    CommentComponent,
    JoinComponent,
    LoginComponent,
    PhotoComponent,
    PostComponent,
    TodoComponent,
    UserComponent,
    UserAddComponent,
    UserChangeComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken'
    }),
    RouterModule.forRoot(ROUTES, {
      useHash: true
    })
  ],
  providers: [
    UserService,
    UserDetailResolver,
    UserListResolver
  ],
  bootstrap: [ 
    AppComponent 
  ]
})
export class AppModule {}
