import { Routes } from '@angular/router';

import {
  UserDetailResolver,
  UserListResolver
} from './resolvers';

import {
  AdminComponent,
  JoinComponent,
  LoginComponent,
  UserAddComponent,
  UserChangeComponent,
  UserComponent,
  UserListComponent
} from './components';

export const ROUTES: Routes = [
  {
    path: 'join',
    component: JoinComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: 'add',
        component: UserAddComponent
      },
      {
        path: ':userId/change',
        component: UserChangeComponent,
        resolve: {
          user: UserDetailResolver
        }
      },
      {
        path: '',
        component: UserListComponent,
        resolve: {
          users: UserListResolver
        }
      }
    ]
  },
  {
    path: '',
    redirectTo: 'admin',
    pathMatch: 'full'
  }
];
